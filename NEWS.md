
rOPTRAM 0.1.0 (2024-04-30)
=========================

INITIAL RESPONSE TO REVIEWS

### NEW FEATURES

  * Implements three curve fitting algorithms to determine OPTRAM trapezoid.

### MINOR IMPROVEMENTS

  * Removes unnecessary tests.
  * Requires user to input area of interest as \code{sf} object.
  * Plot of trapezoid returned as \code{ggplot2} plot to allow user to apply further tweaks.

### BUG FIXES

  * Fixes path to images in vignettes

### DOCUMENTATION FIXES

  * Fixes documentation regarding download with \code{CDSE} package
  * Edits in keeping with [r-lib guide](https://roxygen2.r-lib.org/articles/formatting.html).

